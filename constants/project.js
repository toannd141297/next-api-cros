import { BiDevices } from "react-icons/bi";

const projectsData = [
  {
    title: "S-ONE Wallet",
    link: "https://s-one.finance/",
    thumbnail: "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/s-one-1.png",
    imgUrl: [
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/s-one.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/s-one-2.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/s-one-3.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/s-one-4.png",
    ],
    desc: "Some Contents ",
  },
  {
    title: "Bitcastle Web Design",
    link: "https://bo.bitcastle.io/exchange",
    thumbnail: "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/bitcastle-1.png",
    imgUrl: ["https://toannadi.s3.ap-southeast-1.amazonaws.com/images/bitcastle.png", "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/bitcastle-2.png"],
    desc: "",
  },
  {
    title: "Profile company",
    link: "https://pacom-solution.com/",
    thumbnail: "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom.png",
    imgUrl: [
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom01.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom02.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom03.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom04.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom05.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom06.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom07.png",
      "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/pacom08.png",
    ],
    desc: "",
  },
  {
    title: "Doan Thi Diem Web Design",
    link: "http://doanthidiem.edu.vn/",
    thumbnail: "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/doan-thi-diem.png",
    imgUrl: ["https://toannadi.s3.ap-southeast-1.amazonaws.com/images/doan-thi-diem.png", "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/doan-thi-diem-1.png"],
    desc: "",
  },
  {
    title: "Xor Web UI",
    link: "https://www.xorinc.uk/",
    thumbnail: "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/xor-1.png",
    imgUrl: ["https://toannadi.s3.ap-southeast-1.amazonaws.com/images/xor-1.png", "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/xor.png"],
    desc: "",
  },
  {
    title: "Vnext Web Seminar-Event",
    link: "https://vnext.vn/ja-jp/event.html",
    thumbnail: "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/vnext.png",
    imgUrl: ["https://toannadi.s3.ap-southeast-1.amazonaws.com/images/vnext.png", "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/vnext-2.png"],
    desc: "",
  },
  {
    title: " Design Website Btech Working",
    link: "https://btech-fe-4cae3av3q-truongtx9.vercel.app/en-US",
    thumbnail: "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/btech.png",
    imgUrl: ["https://toannadi.s3.ap-southeast-1.amazonaws.com/images/btech.png", "https://toannadi.s3.ap-southeast-1.amazonaws.com/images/btech-2.png"],
    desc: "",
  },
];
export default projectsData;
