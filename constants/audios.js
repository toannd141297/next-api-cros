const PROD_URL =
  process.env.NODE_ENV !== "production"
    ? process.env.DEV_URL
    : process.env.PROD_URL;
// const PROD_URL = process.env.PROD_URL;
const audios = {
  result: [
    {
      src: `${PROD_URL}/audio/Bong-Hong-Thuy-Tinh-Buc-Tuong.mp3`,
      title: "Bông Hồng Thủy Tinh - Bức Tường",
      artist: "Tâm Hồn Của Đá (The Soul Of Rock)",
    },
    {
      src: `${PROD_URL}/audio/Con-mua-thang-5-Buc-Tuong.mp3`,
      title: "Cơn mưa tháng 5 - Bức Tường",
      artist: "Đất Việt",
    },
    {
      src: `${PROD_URL}/audio/Con-Duong-Khong-Ten-Buc-Tuong.mp3`,
      title: "Con Đường Không Tên - Bức Tường",
      artist: "Con Đường Không Tên",
    },
    {
      src: `${PROD_URL}/audio/Mat-Den-Buc-Tuong.mp3`,
      title: "Mắt Đen - Bức Tường",
      artist: "Vô Hình",
    },
    {
      src: `${PROD_URL}/audio/Hoa-Ban-Trang-Buc-Tuong.mp3`,
      title: "Hoa Ban Trắng- Bức Tường",
      artist: "Ngày Khác",
    },
    {
      src: `${PROD_URL}/audio/Cha-Va-Con-Buc-Tuong.mp3`,
      title: "Cha và Con - Bức Tường",
      artist: "Nam Châm",
    },
    {
      src: `${PROD_URL}/audio/Tro-Ve-Buc-Tuong.mp3`,
      title: "Trở Về - Bức Tường",
      artist: "Nam Châm",
    },
    {
      src: `${PROD_URL}/audio/Thang-12-Buc-Tuong.mp3`,
      title: "Tháng 12 - Bức Tường",
      artist: "Đất Việt",
    },
  ],
};
export default audios;
