import { BiDevices, BiMinusFront, BiCodeAlt } from "react-icons/bi";

const userData = {
  name: "Toannadi",
  designation: "Design UI/UX, Front-End",
  avatarUrl: "/avatar.png",
  email: "toannadi@gmail.com",
  phone: "+84 969 877 097",
  address: "105 P.To Huu, Ha Dong, Ha Noi.",
  skill: [
    {
      title: "Ui/ux Design",
      icon: <BiDevices fontSize={24} />,
    },
    {
      title: "Graphic Design",
      icon: <BiMinusFront fontSize={24} />,
    },
    {
      title: "Front-End Developer",
      icon: <BiCodeAlt fontSize={24} />,
    },
  ],
  experience: [
    {
      title: "Ui/Ux Designer & Software Developer",
      company: "Work in UnirealChain",
      year: "11/2021",
      companyLink: "https://unirealchain.com/",
      desc: "Designer: Designing UI / UX on web, application platform. ",
      desc2:
        "Developer: Developed for web components (React, HTML and Vuejs) and templates.",
    },
    {
      title: "Ui/Ux Designer & Software Developer",
      company: "Work in Vnext Software",
      year: "01/2020",
      companyLink: "https://vnext.vn/",
      desc: "Designer: Designing UI / UX on web, application platform. ",
      desc2:
        "Developer: Developed for web components (React, HTML and Vuejs) and templates.",
    },
    {
      title: "Internship",
      company: "Teneocto technology",
      year: "2018",
      companyLink: "https://teneocto.io/",
      desc: "Developer: Developed for web components (React, HTML and Vuejs)",
    },
    {
      title: "Designer & Marketing Ads",
      company: "Freelancer",
      year: "07/2017",
      companyLink: "",
      desc: "Designer: branding, image.",
      desc2: "Advertise products through facebook channels, forums ...",
    },
  ],

  education: [
    {
      title: "Student",
      company: "VNU, University of Engineering and Technology",
      year: "2015",
      companyLink: "https://uet.vnu.edu.vn/",
      desc: "Major in Computer Network and Communication. Graduated in 2019.",
    },
    {
      title: "Student",
      company: "A Hai Hau high school",
      year: "2012",
      companyLink: "http://thpt-ahaihau.namdinh.edu.vn/",
      desc: "I'm not like other students.",
    },
  ],
  tools: [
    {
      title: "Figma",
      link: "https://figma.com",
      imgUrl: "images/figma.png",
      desc: "Design tool to create mobile, desktop, website and other applications",
    },
    {
      title: "Visual Studio Code",
      link: "https://code.visualstudio.com/",
      imgUrl: "images/vscode.png",
      desc: "A lightweight and powerful text editor created by Microsoft for multiplatform operating systems",
    },
    {
      title: "Vercel",
      link: "https://vercel.com/",
      imgUrl: "images/vercel.png",
      desc: "Hosting and domain service provider for a website",
    },
    {
      title: "Github",
      link: "https://github.com/",
      imgUrl: "images/github.png",
      desc: "A service for storing repositories with the Version Control System (VCS) feature",
    },
  ],
  resumeUrl:
    "https://toannadi.s3.ap-southeast-1.amazonaws.com/resume/Resume+%26+Portfolio+Nguyen+Duc+Toan.pdf",

  instagram: "https://instagram.com/",
  figma: "https://www.figma.com/@toannadi",
  ui4free: "https://ui4free.com/user/toannadi",
  github: "https://github.com/",
  facebook: "https://facebook.com/toannd.97",
  kofi: "https://ko-fi.com/toannadi",
};

export default userData;
